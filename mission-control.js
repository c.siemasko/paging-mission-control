/**
 * @file Entry point for the project | Analyzes satellite sensor data for alerts
 * @author Christian Siemasko <c.siemasko@gmail.com>
 * @version 1.0.0
 */
// #region modules
const fs                                      = require('fs')
const yargs                                   = require('yargs')
const { EOL }                                 = require('os')
const { parseDate, formatDate, minuteDelta }  = require('./util-functions')
// #endregion

// #region yargs-setup
const args = yargs                
    .option('source', {
        alias: 's', description: 'Full path to data source file. Surround with double quotes (")', type: 'string' })
    .option('minfail', {
        alias: 'm', description: 'Minimum number of sequential failed sensor readings to flag a component (Default: 3)', type: 'number', default: 3 })
    .option('windowminutes', {
        alias: 'w', description: 'Number of minutes the number `minfail` of failed sensor readings must fall within (Default: 5)', type: 'number', default: 5 })
    .help().alias('help', 'h').argv

if (!args.source) {
     console.error('You must provide the full path to the data source file. Example:\nnode mission-control -s="sample-data.dat"')
     process.exit()
} 
if (isNaN(args.minfail)) {
    console.error(`Argument value for 'minfail' provided is not a valid number. Defaulting to 3`)
    args.minfail = 3
}
if (isNaN(args.windowminutes)) {
    console.error(`Argument value for 'windowminutes' provided is not a valid number. Defaulting to 5`)
    args.windowminutes = 5
}
    
if (fs.existsSync(args.source)) main()
else {
    console.error(`The file "${args.source}" does not exist. Please verify the full path to the file is correct`)
    process.exit()
}
// #endregion

function main() {    
    const data    = fs.readFileSync(args.source).toString('utf-8').split(EOL).map(d => rawDataFormat(d)) 
    const results = determineSeverity(data.sort((a, b) => a.timestamp.getTime() - b.timestamp.getTime()))
    console.log(`ALERTS${EOL}${'='.repeat(20)}${EOL}`, JSON.stringify(results, null, 2))
}

// #region Functions
/**
 * Construct raw sensor data into JSON for analysis
 * @param {string} dataString 
 * @returns {Object} Formatted record as an object
 */
function rawDataFormat(dataString) { // Build objects to perform analysis on
    const schema = [
        { index: 0, key: 'timestamp', formatter: v => parseDate(v) }, // moment(v, 'YYYYMMDD HH:mm:ss.SSS').toDate() },
        { index: 1, key: 'satelliteId', formatter: v => parseInt(v) },
        { index: 2, key: 'redHighLimit', formatter: v => parseFloat(v) },
        { index: 3, key: 'yellowHighLimit', formatter: v => parseFloat(v) },
        { index: 4, key: 'yellowLowLimit', formatter: v => parseFloat(v) },
        { index: 5, key: 'redLowLimit', formatter: v => parseFloat(v) },
        { index: 6, key: 'rawValue', formatter: v => parseFloat(v) },
        { index: 7, key: 'component' }
    ]
    const values = dataString.split('|')
    const rawObject = {}
    for(let n in values) {
        const s = schema.find(x => x.index == n)
        rawObject[s.key] = s.formatter ? s.formatter(values[n]) : values[n]
    }
    return rawObject
}

/**
 * 
 * @param {Array} data 
 * @param {Number} satelliteId 
 * @returns {Array} Satellites with Thermostat alerts
 */
function detectThermostatAlert(data, satelliteId, minFail, windowMinutes) {
    const flaggedComponents = []
    const thermostats = data
        .filter(d => d.satelliteId === satelliteId && d.component === 'TSTAT')
            
       if (thermostats.length > 0)
           for(let n in thermostats) {
               if (flaggedComponents.length > 0 && flaggedComponents.find(f => f.satelliteId === thermostats[n].satelliteId && f.component === 'TSTAT'))
                   break
               
               const nextFiveMinutes = thermostats.filter(b => minuteDelta(b.timestamp, thermostats[n].timestamp) <= windowMinutes)
               if (nextFiveMinutes.filter(f => f.rawValue > f.redHighLimit).length >= minFail)
                   flaggedComponents.push({ satelliteId, severity: 'RED HIGH', component: 'TSTAT', timestamp: formatDate(thermostats[n].timestamp) })
           }
    
    return flaggedComponents
}

/**
 * 
 * @param {Array} data 
 * @param {Number} satelliteId 
 * @returns {Array} Satellites with Battery alerts
 */
function detectBatteryAlert(data, satelliteId, minFail, windowMinutes) {
    const flaggedComponents = []
    const batteries = data
        .filter(d => d.satelliteId === satelliteId && d.component === 'BATT')
          
        if (batteries.length > 1)
            for(let n in batteries) {
                if (flaggedComponents.length > 0 && flaggedComponents.find(f => f.satelliteId === batteries[n].satelliteId && f.component === 'BATT'))
                    break            
                
                const nextFiveMinutes = batteries.filter(b => minuteDelta(b.timestamp, batteries[n].timestamp) <= windowMinutes)
          
                if (nextFiveMinutes.filter(f => f.rawValue < f.redLowLimit).length >= minFail)
                    flaggedComponents.push({ satelliteId, severity: 'RED LOW', component: 'BATT', timestamp: formatDate(batteries[n].timestamp) })
            }
    return flaggedComponents
}

/**
 * Analyze ingested data and detect alert conditions present in the data
 * @param {Array} data Data objects, sorted by timestamp
 * @returns {Array} The finalized array of alert objects
 */
function determineSeverity(data) {
    let flaggedComponents = []
    const uniqueSatellites = Array.from(new Set(data.map(b => b.satelliteId)))  
    uniqueSatellites.forEach(s => {
            flaggedComponents = flaggedComponents
                .concat(detectThermostatAlert(data, s, args.minfail, args.windowminutes))
                .concat(detectBatteryAlert(data, s, args.minfail, args.windowminutes))
    })
    return flaggedComponents
}
// #endregion