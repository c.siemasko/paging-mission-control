# Paging Mission Control
## _Enlighten Programming Challenge_
#### **Author**: Christian Siemasko
#### **Email**: c.siemasko@gmail.com
#### **Language**: JavaScript
#### **Runtime**: Node.JS
#### **NPM Modules**: `[yargs]`
-------
## Software Prerequisites
- Node.js (v14+)
- [Download Here](https://nodejs.org/en/download/)

## Global Node Module Requirements
- jest

### jest Intallation
-----------
`npm i -g jest`

### NPM module installation
#### _Required before running!_
- Open a terminal window in the folder the project root folder
- Run the command: `npm install`

## Command Line Arguments
-`-source, -s`

Example: `-s="path/to/file.ext"`
## How to Run (Example) 
`node mission-control -s="__tests__/sample-data.dat"`

## How to Run Unit Tests 
- Install `jest` (instructions above)
- Open a terminal window in the folder the project root folder
- Enter `jest` into the terminal, and press [Enter] 
