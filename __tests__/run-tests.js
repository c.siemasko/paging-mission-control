// #region modules
const fs                          = require('fs')
const { EOL }                     = require('os')
const path                        = require('path')
const { parseDate, formatDate }   = require('../util-functions')
// #endregion

const sampleData = fs.readFileSync(path.join(__dirname, 'sample-data.dat')).toString('utf-8').split(EOL)

// #region Functions
const expectedLineCount = (data, count) =>
    data?.every(d => d?.split('|').length === count) ?? false

const hasBatteryComponents = data => 
    data?.some(d => d?.split('|').splice(-1)[0] === 'BATT' ?? false) ?? false
    
const hasThermostatComponents = data =>
    data?.some(d => d?.split('|').splice(-1)[0] ?? false) ?? false

const datesAreValidLength = (dateStrings, expectedLength) => {
    let vals = dateStrings.map(d => ({ dateString: d, parsed: parseDate(d), formatted: formatDate(parseDate(d)) }))
    return vals?.every(d => d.dateString.length === expectedLength && d.parsed && d.formatted.length === 24) ?? false
}

// #endregion

// #region Tests
describe('Line Splitting Length Check',
    () => void test('Assures the provided strings split by `|` are the expected static count',
        () => {
             expect(expectedLineCount(sampleData, 8)).toEqual(true)
        }
))

describe('Battery Component Checker',
    () => void test('Assures there are some `BATT` components in the data',
        () => {
             expect(hasBatteryComponents(sampleData)).toEqual(true)
        }
))

describe('Thermostat Component Checker',
    () => void test('Assures there are some `TSTAT` components in the data',
        () => {
             expect(hasThermostatComponents(sampleData)).toEqual(true)
        }
))

describe('Sensor Date Length Checker',
    () => void test('Assures all sensor records have a valid date length',
        () => {
             expect(datesAreValidLength(sampleData.map(d => d.split('|')[0]), 21)).toEqual(true)
        }
))
// #endregion