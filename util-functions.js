/**
 * @file Utility functions for mission-control and tests
 * @author Christian Siemasko <c.siemasko@gmail.com>
 * @version 1.0.0
 */

const parseDate = dateTimeString => {
    const [ dateString, timeString ] = dateTimeString.split(' ')
    const [ year, month, date] = [ dateString.substr(0,4), dateString.substr(4,2), dateString.substr(6,2)].map(d => parseInt(d))
    const [ hours, minutes ] = timeString.split(':').map(d => parseInt(d))
    const [ seconds, milliseconds ] = timeString.split(':')[2].split('.').map(d => parseInt(d))
    return new Date(year, month - 1, date, hours, minutes, seconds, milliseconds)
}

/**
 * Utility function to determine difference in minutes between two dates
 * @param {Date} d1 
 * @param {Date} d2 
 * @returns {Number} Difference in minutes between two dates
 */
const minuteDelta = (d1, d2) => Math.abs(d2 - d1) / 1000 / 60

/**
 * Format timestamp parameter of final alert objects to expected format
 * @param {Date} date 
 * @returns {String} The formatted string
 */
const formatDate = d => `${d.getFullYear()}-${d.getMonth()+1 < 10 ? (`0${d.getMonth()+1}`) : d.getMonth()+1}-${d.getDate() < 10 ? (`0${d.getDate()}`) : d.getDate()}T${d.getHours() < 10 ? (`0${d.getHours()}`) : d.getHours()}:${d.getMinutes() < 10 ? (`0${d.getMinutes()}`) : d.getMinutes()}:${d.getSeconds() < 10 ? (`0${d.getSeconds()}`) : d.getSeconds()}.${d.getMilliseconds() < 100 && d.getMilliseconds() > 9 ? (`0${d.getMilliseconds()}`) : d.getMilliseconds() < 10 ? (`00${d.getMilliseconds()}`) : d.getMilliseconds()}Z`

module.exports = { parseDate, formatDate, minuteDelta }
